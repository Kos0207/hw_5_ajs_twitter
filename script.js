let urls = [
  "https://ajax.test-danit.com/api/json/users",
  "https://ajax.test-danit.com/api/json/posts",
];

let requests = urls.map((url) => fetch(url));
let users = [];
let posts = [];

class Card {
  constructor (name, email, title, body, id) {
    this.name = name,
    this.email = email,
    this.title = title,
    this.body = body,
    this.id = id
  }
 render() {
  return `<div class="card" id="card-${this.id}">
  <h2>${this.name}</h2>
  <p>${this.email}</p>
  <h3>${this.title}</h3>
  <p>${this.body}</p>
  <button data-id="${this.id}" class="btn-delete" type="button">DELETE</button>
  
</div>`;
 }
}

Promise.all(requests)
  .then((responses) => {
    return responses;
  })
  .then((responses) => Promise.all(responses.map((r) => r.json())))
  .then((datas) => {
    datas.forEach((data) => {
      if (data[0].userId > 0) {
        posts = data;
      } else {
        users = data;
      }
    });
    users.forEach((user) => {
      posts.forEach((post) => {
        if (post.userId === user.id) {
          let cards = document.getElementById("cards");
          let card = new Card(user.name, user.email, post.title, post.body, post.id);

          cards.innerHTML += card.render();
        }
      });
    });

    document.querySelectorAll(".btn-delete").forEach(button => {
      const postId = button.dataset.id
      // console.log(postId);
      button.addEventListener('click', (postId) => deletePost(postId))
    });
  });

function deletePost(postId){
  if (confirm('Are you sure you want to delete this post?')) {
    fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
      method: 'DELETE'
    })
    .then(response => response.json())
    .then((response) => {
      if(response.status===200) {
        document.getElementById("card-"+postId)

      }
      
    })
  }
}
